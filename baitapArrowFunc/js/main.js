const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

var active = "";
const showListColor = () => {
    var listColorHTML = '';
    colorList.forEach(color => {
        const isActive = active === color ? "active" : ""
        listColorHTML += `<button class='color-button ${color} ${isActive}' onclick="changeColor('${color}')"></button>`
    })
    document.getElementById("colorContainer").innerHTML = listColorHTML;
};

const changeColor = (color) => {
    active = color
    document.getElementById("house").setAttribute("class", `house ${color}`);
    showListColor();
}