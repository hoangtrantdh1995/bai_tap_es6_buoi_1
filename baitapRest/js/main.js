let averageScore1 = () => {
    let math = document.getElementById("inpToan").value * 1;
    let physics = document.getElementById("inpLy").value * 1;
    let chemistry = document.getElementById("inpHoa").value * 1;
    let averageScore = average(math, physics, chemistry)
    document.getElementById("tbKhoi1").innerHTML = averageScore.toFixed(2);
};

let averageScore2 = () => {
    let literature = document.getElementById("inpVan").value * 1;
    let history = document.getElementById("inpSu").value * 1;
    let geography = document.getElementById("inpDia").value * 1;
    let english = document.getElementById("inpEnglish").value * 1;
    let averageScore = average(literature, history, geography, english);
    document.getElementById("tbKhoi2").innerHTML = averageScore.toFixed(2);
};

// hàm tính điểm trung bình sử dụng rest parameter
function average(...score) {
    const totalScore = score.reduce((total, score) => {
        total += score
        return total
    }, 0)
    return totalScore / score.length
};